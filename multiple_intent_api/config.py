from typing import Optional
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    path_to_clf_tarfile: Optional[str] = None
    models_dir: Optional[str] = None
    path_to_data_model: str
    path_to_mappings: Optional[str] = None
    path_to_subject_lookups: Optional[str] = None
    path_to_override_lookups: Optional[str] = None
    run_ner: bool = True
    process_attachments: bool = True
    use_multiple_intent: bool = True
    external_preprocessing_url: Optional[str] = None
    external_entities_url: Optional[str] = None
    external_classification_url: Optional[str] = None
    external_logic_url: Optional[str] = None
    path_to_external_logic_override_concepts: Optional[str] = None 
    
    class Config:
        env_file = ".env"


settings = Settings()
